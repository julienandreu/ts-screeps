import { CreepRole } from '../roles/role.interface';

export type BodyPart = MOVE | WORK | CARRY | ATTACK | RANGED_ATTACK | TOUGH | HEAL | CLAIM;

export interface CreepParams {
  body: BodyPart[];
  role: CreepRole;
}
