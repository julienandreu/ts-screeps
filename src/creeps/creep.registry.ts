import { harvesterParams } from './harvester.creep';
import { upgraderParams } from './upgrader.creep';

export const creepRegistry = [harvesterParams, upgraderParams] as const;
