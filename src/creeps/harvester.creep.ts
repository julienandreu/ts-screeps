import { CreepParams } from './body.interface';
import { CreepRole } from '../roles/role.interface';

export const harvesterParams: CreepParams = {
  body: [MOVE, CARRY, WORK],
  role: CreepRole.HARVESTER,
};
