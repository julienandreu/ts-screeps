import { CreepParams } from './body.interface';
import { CreepRole } from '../roles/role.interface';

export const upgraderParams: CreepParams = {
  body: [MOVE, CARRY, WORK],
  role: CreepRole.UPGRADER,
};
