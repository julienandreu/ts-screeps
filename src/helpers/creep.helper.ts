import { CreepRole } from '../roles/role.interface';
import { BodyPart } from '../creeps/body.interface';
import { creepRegistry } from '../creeps/creep.registry';
import { harvesterParams } from '../creeps/harvester.creep';
import { names, uniqueNamesGenerator } from 'unique-names-generator';

export interface GetBodyPartFromRole {
  (role: CreepRole): BodyPart[];
}

export const getBodyPartFromRole: GetBodyPartFromRole = (role) =>
  creepRegistry.find(({ role: creepRole }) => role === creepRole)?.body || harvesterParams.body;

export interface SpawnCreep {
  (role: CreepRole): void;
}

export const spawnCreep: SpawnCreep = (role) => {
  const maybeSpawn = Object.values(Game.spawns).find(Boolean);
  if (!maybeSpawn) {
    console.log(`Spawning Creep error: Please create a new Spawn`);
    return;
  }
  const spawn = maybeSpawn as StructureSpawn;

  const name = `${uniqueNamesGenerator({
    dictionaries: [names],
  })} - ${role}`;
  const body = getBodyPartFromRole(role);
  const cost = body.reduce((totalCost, bodyPart) => totalCost + BODYPART_COST[bodyPart], 0);

  if (spawn.store[RESOURCE_ENERGY] <= cost) {
    console.log(`Missing ${spawn.energy - cost} energy to create new ${role} creep`);
    return;
  }

  const operationCode = spawn?.spawnCreep(body, name, {
    memory: {
      role,
    },
  });

  const operationMessage = ((): string => {
    switch (operationCode) {
      case -1:
        return 'You are not the owner of this spawn.';
      case -3:
        return 'There is a creeps with the same name already.';
      case -4:
        return 'The spawn is already in process of spawning another creeps.';
      case -6:
        return 'The spawn and its extensions contain not enough energy to create a creeps with the given body.';
      case -10:
        return 'Body is not properly described or name was not provided.';
      case -14:
        return 'Your Room Controller level is insufficient to use this spawn.';
      case 0:
      default:
        return 'The operation has been scheduled successfully.';
    }
  })();

  console.log(`Spawning Creep "${name}": ${operationMessage}`);
};

export interface CountCreeps {
  (creeps: typeof Game.creeps): {
    [role: string]: number;
  };
}

export const countCreeps: CountCreeps = (creeps) =>
  Object.values(creeps).reduce(
    (count, { memory: { role: memoryRole } }) => {
      const role = memoryRole as CreepRole;
      const countRole = count[role] || 0;
      return {
        ...count,
        [role]: countRole + 1,
      };
    },
    {
      [CreepRole.HARVESTER]: 0,
      [CreepRole.UPGRADER]: 0,
    },
  );
