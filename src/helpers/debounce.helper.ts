export interface DebounceCallback {
  (): any;
}

export interface Debounce {
  (callback: DebounceCallback, ticks: number, exception: boolean): any;
}

export const debounce: Debounce = (callback, ticks, exception) => {
  if (Game.time % ticks === 0 || exception) {
    return callback();
  }
};
