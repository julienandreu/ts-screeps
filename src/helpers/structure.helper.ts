export interface FindNearestStructure {
  (creep: Creep, structureType: StructureConstant): Structure | null;
}

export const findNearestStructure: FindNearestStructure = (creep, structureType) =>
  creep.pos.findClosestByPath(FIND_STRUCTURES, {
    filter: ({ structureType: type }) => type === structureType,
  });
