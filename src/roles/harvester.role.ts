import { RoleInterface } from './role.interface';
import { findNearestStructure } from '../helpers/structure.helper';

interface Observation {
  focus: string | undefined;
  isHandlingEnergy: boolean;
  hasFreeCapacity: boolean;
  nearestAllyCreep: Creep | null;
  nearestActiveSource: Source | null;
  nearestStructure: Structure | null;
  nearestStructures: {
    [STRUCTURE_EXTENSION]: Structure | null;
    [STRUCTURE_RAMPART]: Structure | null;
    [STRUCTURE_ROAD]: Structure | null;
    [STRUCTURE_SPAWN]: Structure | null;
    [STRUCTURE_LINK]: Structure | null;
    [STRUCTURE_WALL]: Structure | null;
    [STRUCTURE_KEEPER_LAIR]: Structure | null;
    [STRUCTURE_CONTROLLER]: Structure | null;
    [STRUCTURE_STORAGE]: Structure | null;
    [STRUCTURE_TOWER]: Structure | null;
    [STRUCTURE_OBSERVER]: Structure | null;
    [STRUCTURE_POWER_BANK]: Structure | null;
    [STRUCTURE_POWER_SPAWN]: Structure | null;
    [STRUCTURE_EXTRACTOR]: Structure | null;
    [STRUCTURE_LAB]: Structure | null;
    [STRUCTURE_TERMINAL]: Structure | null;
    [STRUCTURE_CONTAINER]: Structure | null;
    [STRUCTURE_NUKER]: Structure | null;
    [STRUCTURE_FACTORY]: Structure | null;
    [STRUCTURE_INVADER_CORE]: Structure | null;
    [STRUCTURE_PORTAL]: Structure | null;
  };
}

interface Observe {
  (creep: Creep): Observation;
}

const observe: Observe = (creep) => ({
  focus: creep.memory.focus,
  isHandlingEnergy: creep.store[RESOURCE_ENERGY] > 0,
  hasFreeCapacity: creep.store.getFreeCapacity() > 0,
  nearestAllyCreep: creep.pos.findClosestByPath(FIND_MY_CREEPS),
  nearestActiveSource: creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE),
  nearestStructure: creep.pos.findClosestByPath(FIND_STRUCTURES),
  nearestStructures: {
    [STRUCTURE_EXTENSION]: findNearestStructure(creep, STRUCTURE_EXTENSION),
    [STRUCTURE_RAMPART]: findNearestStructure(creep, STRUCTURE_RAMPART),
    [STRUCTURE_ROAD]: findNearestStructure(creep, STRUCTURE_ROAD),
    [STRUCTURE_SPAWN]: findNearestStructure(creep, STRUCTURE_SPAWN),
    [STRUCTURE_LINK]: findNearestStructure(creep, STRUCTURE_LINK),
    [STRUCTURE_WALL]: findNearestStructure(creep, STRUCTURE_WALL),
    [STRUCTURE_KEEPER_LAIR]: findNearestStructure(creep, STRUCTURE_KEEPER_LAIR),
    [STRUCTURE_CONTROLLER]: findNearestStructure(creep, STRUCTURE_CONTROLLER),
    [STRUCTURE_STORAGE]: findNearestStructure(creep, STRUCTURE_STORAGE),
    [STRUCTURE_TOWER]: findNearestStructure(creep, STRUCTURE_TOWER),
    [STRUCTURE_OBSERVER]: findNearestStructure(creep, STRUCTURE_OBSERVER),
    [STRUCTURE_POWER_BANK]: findNearestStructure(creep, STRUCTURE_POWER_BANK),
    [STRUCTURE_POWER_SPAWN]: findNearestStructure(creep, STRUCTURE_POWER_SPAWN),
    [STRUCTURE_EXTRACTOR]: findNearestStructure(creep, STRUCTURE_EXTRACTOR),
    [STRUCTURE_LAB]: findNearestStructure(creep, STRUCTURE_LAB),
    [STRUCTURE_TERMINAL]: findNearestStructure(creep, STRUCTURE_TERMINAL),
    [STRUCTURE_CONTAINER]: findNearestStructure(creep, STRUCTURE_CONTAINER),
    [STRUCTURE_NUKER]: findNearestStructure(creep, STRUCTURE_NUKER),
    [STRUCTURE_FACTORY]: findNearestStructure(creep, STRUCTURE_FACTORY),
    [STRUCTURE_INVADER_CORE]: findNearestStructure(creep, STRUCTURE_INVADER_CORE),
    [STRUCTURE_PORTAL]: findNearestStructure(creep, STRUCTURE_PORTAL),
  },
});

interface Orientation {
  couldHarvest: boolean;
  couldStore: boolean;
  spawn: Structure | null;
  source: Source | null;
}

interface Orient {
  (observation: Observation): Orientation;
}

const orient: Orient = ({
  hasFreeCapacity,
  isHandlingEnergy,
  nearestActiveSource,
  nearestStructures: { [STRUCTURE_SPAWN]: spawn },
}) => ({
  couldHarvest: Boolean(hasFreeCapacity) && Boolean(nearestActiveSource),
  couldStore: isHandlingEnergy && Boolean(spawn),
  spawn,
  source: nearestActiveSource,
});

interface Decision {
  act: Act;
}

interface Decide {
  (orientation: Orientation): Decision;
}

const harvestAction = (maybeSource: Source | null) => {
  if (!maybeSource) {
    console.log(`Missing source`);
    return;
  }
  const source = maybeSource as Source;
  return (creep: Creep) => {
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, { reusePath: 1, visualizePathStyle: { stroke: '#FFDC00' } });
    }
  };
};

const storeAction = (maybeSpawn: Structure | null) => {
  if (!maybeSpawn) {
    console.log(`Missing spawn`);
  }
  const spawn = maybeSpawn as StructureSpawn;
  return (creep: Creep) => {
    if (creep.transfer(spawn, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
      creep.moveTo(spawn, { reusePath: 1, visualizePathStyle: { stroke: '#FF851B' } });
    }
  };
};

const decide: Decide = ({ couldHarvest, couldStore, spawn, source }) => ({
  act: (() => {
    switch (true) {
      case couldHarvest:
        return harvestAction(source);
      case couldStore:
        return storeAction(spawn);
      default:
        return harvestAction(source);
    }
  })() as Act,
});

interface Action {
  (creep: Creep): void;
}

interface ActionHarvest extends Action {}

interface ActionStore extends Action {}

type Act = ActionHarvest | ActionStore;

export const harvesterRole: RoleInterface = (creep) => {
  // Senses
  const observation = observe(creep);
  // Brain
  const orientation = orient(observation);
  // Hearth
  const { act } = decide(orientation);
  // Body
  act(creep);
};
