export interface RoleInterface {
  (creep: Creep): unknown;
}

export const CreepRole = {
  HARVESTER: 'harvester',
  UPGRADER: 'upgrader',
} as const;

export type CreepRole = typeof CreepRole[keyof typeof CreepRole];
