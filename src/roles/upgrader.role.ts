import { RoleInterface } from './role.interface';

const harvestEnergy: RoleInterface = (creep) => {};

export const upgraderRole: RoleInterface = (creep) => {
  const {
    memory: { isMining = false, isUpgrading = false },
  } = creep;

  if (isMining || !isUpgrading) {
    if (creep.store.getFreeCapacity() > 0) {
      const source = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
      if (!source) {
        console.log(`Upgrader "${creep.name}" is not able to locate an active Source`);
        return;
      }

      if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
        creep.moveTo(source, { reusePath: 1, visualizePathStyle: { stroke: '#ffffff' } });
      } else {
        creep.memory.isMining = true;
        creep.memory.isUpgrading = false;
      }
      return;
    } else {
      creep.memory.isMining = false;
      creep.memory.isUpgrading = true;
    }
  }

  if (isUpgrading) {
    if (creep.store[RESOURCE_ENERGY] !== 0) {
      // Identify the Controller
      const {
        room: { controller: maybeController },
      } = creep;
      if (!maybeController) {
        console.log(`Upgrader "${creep.name}" is not able to locate a Controller`);
      }
      const controller = maybeController as StructureController;

      // Should harvest if not full

      if (creep.upgradeController(controller) === ERR_NOT_IN_RANGE) {
        creep.moveTo(controller, { reusePath: 1, visualizePathStyle: { stroke: '#f7403a' } });
        return;
      } else {
        creep.memory.isMining = false;
        creep.memory.isUpgrading = true;
      }
    } else {
      creep.memory.isMining = true;
      creep.memory.isUpgrading = false;
    }
  }
};
