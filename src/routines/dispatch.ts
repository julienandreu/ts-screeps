import { harvesterRole } from '../roles/harvester.role';
import { CreepRole, RoleInterface } from '../roles/role.interface';
import { upgraderRole } from '../roles/upgrader.role';

export interface Dispatch {
  (creep: Creep): ReturnType<RoleInterface>;
}

export const dispatch: Dispatch = (creep) => {
  const role: RoleInterface = (() => {
    switch (creep.memory.role) {
      case CreepRole.UPGRADER:
        return upgraderRole;
      case CreepRole.HARVESTER:
      default:
        return harvesterRole;
    }
  })();

  return role(creep);
};
