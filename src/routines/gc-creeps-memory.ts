export const garbageCollectCreepsMemory = () =>
  Object.keys(Memory.creeps)
    .filter((name) => !Game.creeps[name])
    .map((name) => {
      console.log(`Clean memory for Creep "${name}"`);
      delete Memory.creeps[name];
    });
