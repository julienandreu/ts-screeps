import { version } from '../config.json';

export const notifyVersion = () => {
  const { version: previousVersion = '0.0.0' } = Memory;
  if (previousVersion !== version) {
    console.log(`New version pushed v${previousVersion} --> v${version}`);
    Memory.version = version;
  }
};
