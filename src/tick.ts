import { garbageCollectCreepsMemory } from './routines/gc-creeps-memory';
import { notifyVersion } from './routines/notify-version';
import { countCreeps, spawnCreep } from './helpers/creep.helper';
import { dispatch } from './routines/dispatch';
import config from './config.json';
import { creepRegistry } from './creeps/creep.registry';
import { debounce } from './helpers/debounce.helper';

// Optimize memory
// var startCpu = Game.cpu.getUsed();
// var elapsed = Math.round((Game.cpu.getUsed() - startCpu)*Math.pow(10,2))/Math.pow(10,2);
// console.log('Delete memory has used '+elapsed+' CPU time');

export const tick = () => {
  notifyVersion();

  debounce(
    () => {
      const creepRepartition = countCreeps(Game.creeps);
      const creepToSpawn = creepRegistry.find(({ role }) => creepRepartition[role] < (config.pool[role] || 1));
      if (creepToSpawn) {
        spawnCreep(creepToSpawn.role);
      }
    },
    30,
    Object.values(Game.creeps).length === 0,
  );

  Object.values(Game.creeps).map(dispatch);

  garbageCollectCreepsMemory();
};
